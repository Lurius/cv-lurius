<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	"email" => "bones.you.spooky@gmail.com",
        	"password" => Hash::make("QWNVp%VT-ErihSykDPrPeirwn3Urgw"),
        	"name" => "Louis Jean",
        	"surname" => "ARNAUD"
        ]);
    }
}
