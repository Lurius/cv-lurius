<?php

use Illuminate\Database\Seeder;
use App\Models\State;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = ["En cours", "Fini", "Mis en pause", "Abandonné", "Prévus"];

        foreach ($states as $state)
        {
	        State::create([
	        	"libelle" => $state
	        ]);
        }
    }
}
