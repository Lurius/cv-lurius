<?php

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = [
            ["libelle" => "PHP", "logo" => "<i class='fab fa-php'></i>"],
            ["libelle" => "JavaScript", "logo" => "<i class='fab fa-js-square'></i>"],
            ["libelle" => "Python", "logo" => "<i class='fab fa-python'></i>"],
            ["libelle" => "SQL", "logo" => "<i class='fas fa-database'></i>"],
            ["libelle" => "Java", "logo" => "<i class='fab fa-java'></i>"],
            ["libelle" => "Bash", "logo" => NULL],
            ["libelle" => "TypeScript", "logo" => NULL],
            ["libelle" => "Laravel", "logo" => "<i class='fab fa-laravel'></i>"],
            ["libelle" => "Symfony", "logo" => NULL],
            ["libelle" => "Angular", "logo" => "<i class='fab fa-angular'></i>"]
        ];

        foreach ($languages as $language) {
            Language::insert($language);
        }
    }
}
