<?php

use App\Models\Section;
use Illuminate\Database\Seeder;

class SectionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Section::create([
            "title" => "Qui suis-je ?",
            "content" => "Je me nomme Louis Jean ARNAUD. Je suis actuellement un étudiant du lycée Simone Weil, en deuxième année de BTS SIO. <br>
(Brevet de Technicien Supérieur en Services Informatiques aux Organisations)",
        ]);

        Section::create([
            "title" => "Auto-description",
            "content" => "Je suis un homme passionné de nombreux sujet, allant de l'informatique à la littérature en passant par la psychologie et le jeu vidéo.<br>
Je me défini comme une personne franche, n'ayant pas peur de la prise de parole sur de nombreux sujet ni de reconnaître ces torts, bien qu'étant souvent novice sur les thèmes que j'abordes.<br>
<br>
J'ai tendances à vouloir comprendre certains choix, dans un cahier des charges par exemple, me semblant douteux et à questionner mes interlocuteurs, ou ma propre personne, sur les marches corrects à suivre.<br> Bien que je sache me recadrer si la situation l'exige et, pense savoir, accordé ma confiance vis-à-vis des choix effectué dans les moments de crises.<br>
<br>
Je ne me défini cependant pas comme une personne avec une facilité d'apprentissage, bien qu'aillant peu d'expérience pour le savoir, mais je pense être assez téméraires pour y parvenir.<br>
Je trouve également avoir un rythme de travail correct bien qu'il, de par mon pessimisme envers ma personne, me semble assez lent.<br>
<br>
De plus, mon éthique me pousse à préciser, au moins dans la documentation, les possibles reprises d'élément d'autres personnes.<br>
<br>
Enfin, mon perfectionnisme me pousse à parfois vouloir reprendre les bases d'éléments dont je ne comprend pas l'assemblage, voir me pousser à questionner les personnes pouvant m'aider dans ce cadre.<br> Bien que je sache me recentrer sur ce qui m'est demandé.<br>",
        ]);

        Section::create([
            "title" => "Raison de ce site",
            "content" => "J'ai réalisé ce site dans le but de faire une refonte de la première version de mon portfolio.<br>
Ce portfolio comporte en effet bien plus d'éléments : Une gestion des connexions, des commentaires et une présentation plus claire.",
        ]);
    }
}
