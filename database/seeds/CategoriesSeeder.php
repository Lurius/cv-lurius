<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ["Projet scolaire", "Projet personnel", "Projet de stage"];

        foreach ($categories as $categorie)
        {
	        Category::create([
	        	"libelle" => $categorie
	        ]);
        }
    }
}
