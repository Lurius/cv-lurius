<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Ecran d'acceuil
Route::get('/', "PortfolioController@index")->name('folio.index');

// Pages ne comprenant que du texte
Route::get('/presentation', "PortfolioController@presentation")->name('folio.presentation');
Route::get('/competence', "PortfolioController@competence")->name('folio.competence');
Route::get('/contact', "PortfolioController@contact")->name('folio.contact');

// Blog sur mes projets
Route::get('/project', "ProjectController@index")->name('project.index');
Route::get('/project/show/{id}', "ProjectController@show")->name('project.show');
Route::get('/project/create', "ProjectController@create")->name('project.create');
Route::post('/project/create', "ProjectController@register")->name('project.register');
Route::get('/project/edit/{id}', "ProjectController@update")->name('project.update');
Route::patch('/project/edit/{id}', "ProjectController@save")->name('project.save');
Route::delete('/project/delete/{id}', "ProjectController@delete")->name('project.delete');

// Gestion des messages
Route::post('/comment/{project_id}', "CommentController@store")->name('comment.store');
Route::patch('/comment/edit/{id}', "CommentController@update")->name('comment.update');
Route::delete('/comment/delete/{id}', "CommentController@delete")->name('comment.delete');

// Gestion des réponses
Route::post('/response/{comment_id}/{project_id}', "ResponseController@store")->name('response.store');
Route::patch('/response/edit/{id}', "ResponseController@update")->name('response.update');
Route::delete('/response/delete/{id}', "ResponseController@delete")->name('response.delete');

// Gestion des connexion
Route::get('/connexion', "UserController@index")->name('log.index');
Route::get('/connexion/new', "UserController@create")->name('log.new');
Route::post('/connexion/new', "UserController@register")->name('log.register');
Route::post('/connexion', "UserController@login")->name('log.login');
Route::get('/deconnexion', "UserController@logout")->name('log.logout');

// Gestion des utilisateurs
Route::get('/account', "UserController@show")->name('user.show');
Route::get('/account/modify', "UserController@update")->name('user.update');
Route::post('/account/modify', "UserController@save")->name('user.save');
Route::delete('/account/{id}', "UserController@delete")->name('user.delete');
