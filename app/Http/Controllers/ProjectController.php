<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Language;
use App\Models\Project;
use App\Models\State;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProjectController extends Controller
{
    /***
     * Affiche la liste de tous les projet, avec une barre de recherche pour trier les différents projets.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $projects = Project::orderBy("updated_at", "DESC")->with(["languages", "state", "category"])->get();
        $admin = User::where("email", "=", "bones.you.spooky@gmail.com")->first();

        // $dates = Project::orderBy("created_at", "ASC")->pluck("created_at")->unique(function($item) { return $item->year; });

        return view('cv.projects.index', compact(["projects", "admin"]));
    }

    /***
     * Affiche un projet précis et ces commentaires
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(int $id)
    {
        $project = Project::with("comments.user", "comments.responses.user")->where("id", "=", $id)->first();
        $admin = User::where("email", "=", "bones.you.spooky@gmail.com")->first();

        return view('cv.projects.show', compact(["project", "admin"]));
    }

    /***
     * Affiche le formulaire de création d'un projet
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $admin = User::where("email", "=", "bones.you.spooky@gmail.com")->first();

        if (Session::has("user") && Session::get("user")->id == $admin->id) {
            $states = State::all();
            $categories = Category::all();
            $languages = Language::all();

            return view('cv.projects.create', compact(["states", "categories", "languages"]));
        } else {
            return redirect(route('folio.index'));
        }
    }

    /***
     * Enregistre le nouveau projet
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register(Request $request)
    {
        $request->validate([
            "title" => "required|min:5|max:255",
            "resume" => "required|min:5",
            "content" => "required|min:5",
            "state" => "required",
            "category" => "required",
        ]);

        $project = Project::create([
            "title" => $request->input("title"),
            "resume" => $request->input("resume"),
            "content" => $request->input("content"),
            "state_id" => $request->input("state"),
            "category_id" => $request->input("category"),
        ]);

        $project->languages()->attach($request->input("languages"));

        return redirect()->route("project.index");
    }

    /**
     * Enregistre les modification sur le projet
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(int $id)
    {
        $states = State::all();
        $categories = Category::all();
        $project = Project::with("comments.user", "comments.responses.user")->where("id", "=", $id)->first();
        $languages = Language::all();

        return view("cv.projects.update", compact(["states", "categories", "project", "languages"]));
    }

    /**
     * Enregistre les modification sur le projet
     *
     * @param Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, int $id)
    {
        $request->validate([
            "title" => "required|min:5|max:255",
            "resume" => "required|min:5",
            "content" => "required|min:5",
            "state" => "required",
            "category" => "required",
        ]);

        $project = Project::find($id);

        $project->update([
            "title" => $request->input("title"),
            "resume" => $request->input("resume"),
            "content" => $request->input("content"),
            "state_id" => $request->input("state"),
            "category_id" => $request->input("category"),
        ]);

        $project->languages()->detach();
        $project->languages()->attach($request->input("languages"));

        return redirect()->route("project.index");
    }

    /**
     * Suppprime le projet et ces commentaires associés
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(int $id)
    {
        $project = Project::find($id);

        $project->delete();

        return redirect()->route("project.index");
    }
}
