<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Models\User;

class UserController extends Controller
{
	/**
	Affiche le formulaire de connexion / enregistrement (changement avec un bouton)
	*/
    public function index()
    {
    	return view("user.index");
    }


    /**
    Envoie sur la page de création d'utilisateur
    */
    public function create()
    {
        return view("user.create");
    }

	/**
	Enregistre le nouvel utilisateur
	*/
    public function register(Request $request)
    {

        $request->validate([
            "email"         =>  "required|min:1|max:255|email",
            "name"          =>  "required|min:1|max:255",
            "surname"       =>  "required|min:1|max:255",
            "password"      =>  "required|min:1|confirmed",
        ]);

        if(!User::where('email', '=', $request->email)->first())
        {
            User::create([
                "email"         =>  $request->email,
                "name"          =>  $request->name,
                "surname"       =>  $request->surname,
                "password"      =>  Hash::make($request->password),
            ]);

            return redirect()->route('log.index');
        }

        return redirect()->back();
    }

    /**
    Vérifie la connexion et :
    	- Si le visiteur n'a pas donnée un bon mdp ou pseudo
    		-> retourne une erreur
    	- Si le visiteur a donnée un bon mdp et pseudo
    		-> renvoie le visiteur sur la page où il se trouvait
	*/
    public function login(Request $request)
    {
        $user = User::where('email', '=', $request->input('email'))->first();
        if($user and Hash::check($request->input('password'), $user->password))
        {
            Session::put('user', $user);
            return redirect()->route('folio.index');
        }
        
        return back();
    }

    /**
    Déconnecte l'utilisateur actuel et redirige sur la page où se trouvait le visiteur
	*/
    public function logout()
    {
        Session::flush();
    	return redirect()->back();
    }

    /**
	Retourne le profil du visiteur
    */
    public function show()
    {
        if(Session::has('user'))
        {
            return view('user.show');
        }
        else
        {
            return redirect()->route('folio.index');
        }
    }

    /**
    Affiche le formulaire de modification du visiteur
	*/
    public function update()
    {
        
    	if(Session::has('user'))
        {
            return view('user.update');
        }
        else
        {
            return redirect()->route('folio.index');
        }
    }
    
    /**
    Enregistre les modification du visiteur
	*/
    public function save(Request $request)
    {
        $request->validate([
            "email"         =>  "required|min:1|max:255|email",
            "name"          =>  "max:255",
            "surname"       =>  "max:255",
            "oldPassword"   =>  "required|min:1",
            "password"      =>  "confirmed",
        ]);

        $user = User::where('email', '=', $request->email)->first();
        if(Hash::check($request->input('oldPassword'), $user->password))
        {
            if ($request->name) 
            {
                $user->update([
                    "name"  =>  $request->name,
                ]);
            }

            if ($request->surname) 
            {
                $user->update([
                    "surname" => $request->surname,
                ]);
            }

            if ($request->password and Hash::check($request->oldPassword, $user->password)) 
            {
                $user->update([
                    "password" => Hash::make($request->password),
                ]);
            }
        }
        else
        {
            return redirect()->back();
        }

    	return redirect()->route('folio.index');
    }

    /**
    Supprime le compte de l'utilisateur
	*/
    public function delete(int $id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        Session::flush();

    	return redirect()->route('folio.index');
    }
}
