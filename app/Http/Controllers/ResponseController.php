<?php

namespace App\Http\Controllers;

use App\Models\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ResponseController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param int $comment_id
     * @param int $project_id
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, int $comment_id, int $project_id)
    {
        $request->validate([
            "content" => "required|min:1"
        ]);

        Response::create([
            "content" => $request->input('content'),
            "user_id" => Session::get("user")->id,
            "comment_id" => $comment_id
        ]);

        return redirect()->route('project.show', $project_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            "content" => "required|min:1"
        ]);

        $response = Response::findOrFail($id);

        $response->update([
            "content" => $request->input('content'),
        ]);

        return redirect()->back();
    }

    /***
     * Delete the specified resource in storage
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(int $id)
    {
        $response = Response::findOrFail($id);

        $response->delete();

        return redirect()->back();
    }
}
