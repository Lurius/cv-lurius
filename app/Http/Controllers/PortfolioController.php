<?php

namespace App\Http\Controllers;

use App\Models\Competence;
use App\Models\Language;
use App\Models\Section;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
	/**
	Affiche la page d'acceuil
	*/
    public function index()
    {
    	return view('cv/index');
    }

    /**
    Affiche la page de présentation
    */
    public function presentation()
    {
        $sections = Section::all();

    	return view('cv/presentation', compact("sections"));
    }

    /**
    Affiche page de compétence, avec différente liste
    */
    public function competence()
    {
    	//Récupération et envoie de la liste des langages de programmation et développement
        $languages = Language::all();
        $competences = Competence::all();

    	return view('cv/competence', compact("languages", "competences"));
    }

    /**
    Affiche mon addresse, e-mail, un liens permettant l'envoie et un formulaire en cas de problème sur le site
    */
    public function contact()
    {
    	return view('cv/contact');
    }
}
