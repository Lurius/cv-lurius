<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param int $project_id
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, int $project_id)
    {
        $request->validate([
            "content" => "required|min:1"
        ]);

        Comment::create([
            "content" => $request->input('content'),
            "user_id" => Session::get("user")->id,
            "project_id" => $project_id
        ]);

        return redirect()->route('project.show', $project_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            "content" => "required|min:1"
        ]);

        $comment = Comment::findOrFail($id);

        $comment->update([
            "content" => $request->input('content'),
        ]);

        return redirect()->back();
    }

    /***
     * Delete the specified resource in storage
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(int $id)
    {
        $comment = Comment::findOrFail($id);

        $comment->delete();

        return redirect()->back();
    }
}
