<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // Doesn't have "created_at" and "update_at"
    public $timestamps = false;

    protected $fillable = [
    	"libelle",
    ];
}
