<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Comment;

class User extends Model
{
    protected $fillable = [
    	"email",
    	"password",
    	"name",
    	"surname",
    ];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
