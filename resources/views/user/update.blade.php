@extends("cv.includes.template")

@section("content")
<form class="card" method="POST" action="{{ route('user.save') }}">
  {{ csrf_field() }}
  <p class="card-header">Changer vos données personnels</p>
  <div class="card-body">
    <label for="email">Adresse mail du compte :</label>
    <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="{{ Session::get('user')->email }}">
    <small id="emailHelp" class="form-text text-muted">* Obligatoire</small>

    <label for="oldPassword">Mot de passe :</label>
    <input type="password" name="oldPassword" class="form-control" id="oldPassword" placeholder="Mot de passe actuel">
    <small id="passwordHelp" class="form-text text-muted">* Obligatoire</small>
  
    <hr>

    <label for="name">Prénom :</label>
    <input type="text" name="name" class="form-control" id="name" placeholder="{{ Session::get('user')->name }}">

    <label for="surname">Nom :</label>
    <input type="text" name="surname" class="form-control" id="surname" placeholder="{{ Session::get('user')->surname }}">

    <hr>

    <label for="newPassword">Nouveau mot de passe :</label>
    <input type="password" name="password" class="form-control" id="newPassword" placeholder="Nouveau mot de passe">
    <small id="passwordHelp" class="form-text text-muted">Pour plus de sécurité mettre des caractères spéciaux (@, &, *, ...)</small>

    <label for="newPassword">Confirmation du nouveau mot de passe :</label>
    <input type="password" name="password_confirmation" class="form-control" id="newPassword" placeholder="Nouveau mot de passe">
  </div>
  <div class="card-footer text-center">
    <button type="submit" class="btn btn-outline-dark">Submit</button>
  </div>
</form>
    
@endsection