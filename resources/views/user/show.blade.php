@extends("cv.includes.template")

@section("content")
<div class="card">
	<div class="card-header">
		Votre compte :
	</div>
	<div class="card-body">
		
		Email : {{ Session::get('user')->email }}
		<hr>
		
		Prénom : {{ Session::get('user')->name }}
		<hr>

		Nom : {{ Session::get('user')->surname }}
	</div>
	<div class="card-footer">
		<a href="{{ route('user.update') }}" role="button" class="btn btn-dark">
			Modifier
		</a>

		<form method="POST" action="{{ route('user.delete', [Session::get('user')->id]) }}">
			{{ csrf_field() }}
			{{ method_field("DELETE") }}

			<button class="btn btn-danger" type="submit">
				Supprimer
			</button>

		</form>
	</div>
</div>
@endsection
