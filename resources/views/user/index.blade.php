@extends("cv.includes.connexion")

@section('content')
<div class="fixed-top">
  <a class="btn btn-dark" type="button" href="{{ route('folio.index') }}">Retourner sur le portfolio</a>
</div>
  <form class="form-signin" method="POST" action="{{ route('log.login') }}">
    {{ csrf_field() }}
    <h1 class="h3 mb-3 font-weight-normal">Connexion :</h1>
    <label for="inputEmail" class="sr-only">Adresse mail</label>
    <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Adresse mail" required autofocus>
    <label for="inputPassword" class="sr-only">Mot de passe</label>
    <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Mot de passe" required>
    <div class="checkbox mb-3">
      {{-- <label>
        <input type="checkbox" value="remember-me"> Remember me
      </label> --}}
      <a class="btn btn-block" href="{{ route('log.new') }}">Créer un compte</a>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Connexion</button>
  </form>
@endsection