@extends("cv.includes.connexion")

@section('content')
<form class="form-signin" method="POST" action="{{ route('log.register') }}">
{{ csrf_field() }}
	<h1 class="h4 mb-3 font-weight-normal">Créer un nouveau compte</h1>
		<label for="email">Adresse mail du compte * :</label>
		<input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Adresse mail">

		<label for="name">Prénom :</label>
		<input type="text" name="name" class="form-control" id="name" placeholder="Prénom">

		<label for="surname">Nom :</label>
		<input type="text" name="surname" class="form-control" id="surname" placeholder="Nom">
		
		<label for="oldPassword">Mot de passe * :</label>
		<input type="password" name="password" class="form-control" id="password" placeholder="Mot de passe">

		<label for="oldPassword">Confirmation du mot de passe * :</label>
		<input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Mot de passe">

    	<button type="submit" class="btn btn-lg btn-primary btn-block">Submit</button>

		<small id="passwordHelp" class="form-text text-muted">* Obligatoire</small>
</form>
@endsection