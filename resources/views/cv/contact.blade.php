@extends("cv.includes.template")

@section("content")
    <div class="card mb-2 row col-12 p-0">
        <div class="card-header d-flex justify-content-center align-items-center">
            <h1 class="title">Mon adresse mail pro :</h1>
        </div>
        <div class="card-body d-flex justify-content-center align-items-center">
            <a>louis.arnaud.pro@gmail.com</a>
        </div>
    </div>

    <div class="card mb-2 row col-12 p-0">
        <div class="card-header d-flex justify-content-center align-items-center">
            <h1 class="title">Délai d'attente :</h1>
        </div>
        <div class="card-body d-flex justify-content-center align-items-center">
            Étant encore un étudiant en Brevet de Technicien Supérieur des Services Informatiques aux Organisations à
            Simone Weil, il se pourrait que je ne réponde pas avant 18/19h aux propositions et mails envoyés.
            <br>
            <br>
            Cordialement,
            <br>
            Louis J. ARNAUD
        </div>
    </div>
@endsection

@section("size_con")
    h-75
@endsection
