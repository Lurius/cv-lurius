@extends("cv.includes.template")

@section("content")
    @if(Session::has("user"))
        @if(Session::get("user")->id == $admin->id)
            <a href="{{ route("project.update", $project->id) }}" class="btn btn-dark">
                <i class="fas fa-retweet"></i> Modifier ce projet
            </a>
        @endif
    @endif
    <div class="card mb-3 mt-3">
        <div class="card-header row mx-0">
            <h5 class="card-title"> {{ $project->title }} </h5>
            <p class="card-text col" style="text-align: right;">
                <small class="text-muted">
                    Modifié {{ Carbon\Carbon::parse($project->updated_at)->diffForHumans() }}
                </small>
            </p>
        </div>
        <div class="card-header">
            {!! $project->resume !!}
        </div>
        <div class="card-body">
            <p class="card-text">
                {!! $project->content !!}
            </p>
        </div>
        <div class="card-footer row mx-0">
            <a href="{{ route("project.index") }}" class="btn btn-dark my-2 my-sm-0">
                <i class="fas fa-arrow-alt-circle-left"></i> Retourner sur la liste des projets
            </a>
            <p class="card-text col" style="text-align: right;">
                <small class="text-muted">
                    Créé {{ Carbon\Carbon::parse($project->created_at)->diffForHumans() }}
                </small>
            </p>

            @if(Session::has("user"))
                <div class="card mt-3 col-12 mx-0 px-0">
                    <div class="card-header row mx-0">
                        <h6 class="card-title">Ajouter un commentaire</h6>
                    </div>
                    <form method="POST" action="{{ route("comment.store", $project->id) }}">
                        @csrf
                        <div class="card-body row mx-0 p-0">
                            <textarea class="col-12" name="content" id="content" title="Nouveau commentaire"></textarea>
                        </div>
                        <div class="card-footer row mx-0">
                            <button type="submit" class="btn btn-dark">Ajouter le commentaire</button>
                        </div>
                    </form>
                </div>
            @endif

            @foreach($project->comments as $comment)
                <div class="card mt-3 col-12 mx-0 px-0">
                    <div class="card-header row mx-0">
                        {{ $comment->user->surname }} {{ $comment->user->name }}
                        <p class="card-text col" style="text-align: right;">
                            <small class="text-muted">
                                Modifié {{ Carbon\Carbon::parse($comment->updated_at)->diffForHumans() }}</small>
                        </p>
                    </div>
                    @if(Session::has("user"))
                        @if(Session::get("user")->id == $comment->user->id or Session::get("user")->id == $admin->id)
                            <form method="POST" action="{{ route("comment.update", $comment->id) }}">
                                @csrf
                                @method('PATCH')
                                <div class="card-body">
                                    <textarea class="form-control" name="content"
                                              id="content">{{ $comment->content }}</textarea>
                                </div>
                                <div class="card-footer row mx-0">
                                    <button type="submit" class="btn btn-dark">
                                        Modifier
                                    </button>
                            </form>
                            <form method="POST" action="{{ route("comment.delete", $comment->id) }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">
                                    Supprimer
                                </button>
                            </form>
                        @endif
                    @else
                        <div class="card-body">
                            {{ $comment->content }}
                        </div>
                        <div class="card-footer row mx-0">
                            @endif
                            <p class="card-text col" style="text-align: right;">
                                <small class="text-muted">
                                    Créé {{ Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}
                                </small>
                            </p>
                            @if(Session::has("user"))
                                <div class="card mt-3 col-12 mx-0 px-0">
                                    <div class="card-header row mx-0">
                                        <h6 class="card-title">Répondre</h6>
                                    </div>
                                    <form method="POST"
                                          action="{{ route("response.store", [$comment->id, $project->id]) }}">
                                        @csrf
                                        <div class="card-body row mx-0 p-0">
                                            <textarea class="col-12" name="content" id="content"
                                                      title="Nouveau commentaire"></textarea>
                                        </div>
                                        <div class="card-footer row mx-0">
                                            <button type="submit" class="btn btn-dark">Ajouter la réponse</button>
                                        </div>
                                    </form>
                                </div>
                            @endif
                            @if($comment->responses)
                                @foreach($comment->responses as $response)
                                    <div class="card col-12 mx-0 px-0">
                                        <div class="card-header row mx-0">
                                            {{ $response->user->surname }} {{ $response->user->name }}
                                            <p class="card-text col" style="text-align: right;">
                                                <small class="text-muted">
                                                    Modifié {{ Carbon\Carbon::parse($response->updated_at)->diffForHumans() }}</small>
                                            </p>
                                        </div>
                                        @if(Session::has("user"))
                                            @if(Session::get("user")->id == $response->user->id or Session::get("user")->id == $admin->id)
                                                <form method="POST"
                                                      action="{{ route("response.update", $response->id) }}">
                                                    @csrf
                                                    @method('PATCH')
                                                    <div class="card-body">
                                                        <textarea class="form-control" name="content"
                                                                  id="content">{{ $response->content }}</textarea>
                                                    </div>
                                                    <div class="card-footer row mx-0">
                                                        <button type="submit" class="btn btn-dark">
                                                            Modifier
                                                        </button>
                                                </form>
                                                <form method="POST"
                                                      action="{{ route("response.delete", $response->id) }}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger">
                                                        Supprimer
                                                    </button>
                                                </form>
                                            @endif
                                        @else
                                            <div class="card-body">
                                                {{ $response->content }}
                                            </div>
                                            <div class="card-footer row mx-0">
                                        @endif
                                                <p class="card-text col" style="text-align: right;">
                                                    <small class="text-muted">
                                                        Créé {{ Carbon\Carbon::parse($response->created_at)->diffForHumans() }}
                                                    </small>
                                                </p>
                                            </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection

@section("size_pro")
    h-75
@endsection
