@extends("cv.includes.template")

@section("content")
    <div class="d-flex flex-column h-100">
        <form method="POST" action="{{ route("project.register") }}">
            @csrf
            <div class="form-group">
                <label for="title">Titre </label>
                <input type="text" name="title" class="form-control" id="title">
            </div>
            <div class="form-group">
                <label for="resume">Résumé</label>
                <textarea class="form-control" name="resume" id="resume" rows="20"></textarea>
            </div>
            <div class="form-group">
                <label for="content">Contenue</label>
                <textarea class="form-control" name="content" id="content" rows="20"></textarea>
            </div>
            <div class="form-group">
                <label for="state">Etat</label>
                <select id="state" name="state" class="form-control">
                    <option selected class="collapse"></option>
                    @foreach($states as $state)
                        <option value="{{ $state->id }}">{{ $state->libelle }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="category">Categorie</label>
                <select id="category" name="category" class="form-control">
                    <option selected class="collapse"></option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->libelle }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="languages">Langages</label>
                <select id="languages" name="languages[]" class="form-control" multiple="multiple">
                    @foreach($languages as $language)
                        <option value="{{ $language->id }}">{{ $language->libelle }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-dark">Submit</button>
        </form>
    </div>
    <!-- Include external JS libs. -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>

@endsection

@section("size_pro")
h-75
@endsection
