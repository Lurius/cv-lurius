@extends("cv.includes.template")

@section("content")
    <form action="{{ route("project.delete", $project->id) }}" method="POST">
        @method('DELETE')
        @csrf
        <button type="submit" class="btn btn-danger">Supprimer</button>
    </form>
    <div class="d-flex flex-column h-100">
        <form method="POST" action="{{ route("project.save", $project->id) }}">
            @csrf
            @method('PATCH')
            <div class="form-group">
                <label for="title">Titre </label>
                <input type="text" name="title" class="form-control" id="title" value="{{ $project->title }}">
            </div>
            <div class="form-group">
                <label for="resume">Résumé</label>
                <textarea class="form-control" name="resume" id="resume" rows="20">{{ $project->resume }}</textarea>
            </div>
            <div class="form-group">
                <label for="content">Contenue</label>
                <textarea class="form-control" name="content" id="content" rows="20">{{ $project->content }}</textarea>
            </div>
            <div class="form-group">
                <label for="state">Etat</label>
                <select id="state" name="state" class="form-control">
                    <option selected class="collapse"
                            value="{{ $project->state_id }}">{{ $project->state->libelle }}</option>
                    @foreach($states as $state)
                        <option value="{{ $state->id }}">{{ $state->libelle }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="category">Categorie</label>
                <select id="category" name="category" class="form-control">
                    <option selected class="collapse"
                            value="{{ $project->category_id }}">{{ $project->category->libelle }}</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->libelle }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="languages">Langages</label>
                <select id="languages" name="languages[]" class="form-control" multiple="multiple">
                    @foreach($languages as $language)
                        @if($project->languages->find($language->id))
                            <option selected value="{{ $language->id }}">{{ $language->libelle }}</option>
                        @else
                            <option value="{{ $language->id }}">{{ $language->libelle }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-dark">Sauvegarder les modification</button>
        </form>
    </div>
    <!-- Include external JS libs. -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>

    <script src="{{ asset("js/simplemde.min.js") }}"></script>
    <script>
        var simplemde = new SimpleMDE({element: document.getElementById("resume")})
    </script>
    <script>
        var simplemde = new SimpleMDE({element: document.getElementById("content")})
    </script>

    <!--new SimpleMDE({
            spellChecker: false
        })
        document.querySelector(`#submit`).addEventListener(`click`, () => {
            document.querySelector(`#form`).submit()
        })-->
@endsection

@section("size_pro")
    h-75
@endsection
