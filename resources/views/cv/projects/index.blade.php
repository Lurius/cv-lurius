@extends("cv.includes.template")

@section("content")
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark rounded filters row">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse navbar-static-top" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                @if(Session::has("user"))
                    @if(Session::get("user")->id == $admin->id)
                        <li class="nav-item">
                            <a href="{{ route("project.create") }}" class="nav-link active">
                                <i class="fas fa-plus"></i> Créer un nouveau projet
                            </a>
                        </li>
                    @endif
                @endif
            </ul>
            <div class="form-inline col">
                <input class="form-control rounded search-box col" type="search" placeholder="Titre"
                       aria-label="Search">
            </div>
        </div>
    </nav>


    @foreach($projects as $project)
        <div class="card mb-3 mt-3 task-list-row">
            <div class="card-header row mx-0">
                <h5 class="card-title col title">
                    {{ $project->title }}
                    <p class="text-hide">
                        @if($project->languages)
                            @foreach($project->languages as $language)
                                {{ $language->libelle }},
                            @endforeach
                        @endif
                        {{ $project->state->libelle }}
                        {{ $project->category->libelle}}
                    </p>
                </h5>
                <p class="card-text col" style="text-align: right;">
                    <small class="text-muted">
                        Modifié {{ Carbon\Carbon::parse($project->updated_at)->diffForHumans() }}
                    </small>
                </p>
            </div>
            <div class="card-header">
                <button class="btn btn-dark">
                    {{ $project->state->libelle }}
                </button>
                <button class="btn btn-dark">
                    {{ $project->category->libelle }}
                </button>
            @if($project->languages)
                    @foreach($project->languages as $language)
                        <button class="btn btn-outline-dark">
                            @if($language->logo)
                                {!! $language->logo !!}
                            @else
                                <i class="fas fa-tags"></i>
                            @endif {{ $language->libelle }}
                        </button>
                    @endforeach
                </div>
            @endif
            <div class="card-body">
                <p class="card-text"> {!! $project->resume !!} </p>
            </div>
            <div class="card-footer text-muted row mx-0">
                <a href="{{ route("project.show", $project->id) }}" class="btn btn-outline-dark">Voir le projet</a>
                <p class="card-text col" style="text-align: right;">
                    <small class="text-muted">
                        Créé {{ Carbon\Carbon::parse($project->created_at)->diffForHumans() }}
                    </small>
                </p>
            </div>
        </div>
    @endforeach

    <script>
        // Filtrer les projets en fonction de la recherche
        function search($) {
            $('.search-box').on('keyup', function () {
                var searchTerm = $(this).val().toLowerCase(); // Contient la valeur de search-box

                $('.task-list-row').each(function () {
                    $row = $(this);
                    let title = $row.find('.card-header .title').html(); // Récupère la valeur du titre

                    if (!$row.hasClass("js-search-excluded") && title.toLowerCase().search(searchTerm) > -1) {
                        $row.show();
                    } else {
                        $row.hide();
                    }
                });
            });
        }

        $(document).ready(search($));
    </script>
@endsection

@section("size_pro")
    h-75
@endsection
