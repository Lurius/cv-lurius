@extends("cv.includes.template")

@section("content")
    <div class="row">
    @foreach($sections as $section)
        <div class="card mb-2 row col-12 p-0">
            <div class="card-header d-flex justify-content-center align-items-center">
                <h1 class="title">{{ $section->title }}</h1>
            </div>
            <div class="card-body d-flex justify-content-center align-items-center">
                {!! $section->content !!}
            </div>
        </div>
    @endforeach
    </div>
@endsection

@section("size_pre")
h-75
@endsection
