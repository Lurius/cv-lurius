<div class="sidebar-sticky">
    <button class="btn btn-link bd-search-docs-toggle p-0 ml-3" type="button" data-toggle="collapse" data-target="#bd-docs-nav" aria-controls="bd-docs-nav" aria-expanded="true" aria-label="Toggle docs navigation" wfd-id="397">
        {{-- <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" width="30" height="30" focusable="false">  --}}
    <i class="fas fa-bars fa-2x" style="color: black;">
        <title>
        Menu
        </title>
        <path stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-miterlimit="10" d="M4 7h22M4 15h22M4 23h22">
        </path>
    </i>
</button>
</div>
<nav class="col-10 col-md-3 col-xl-2 bd-sidebar bd-links collapse show" id="bd-docs-nav">
    <div class="sidebar-sticky">
        <div class="list-group h-100">
            <a class="list-group-item d-flex justify-content-center align-items-center @yield("size_pre") h-25 title" href="{{ route("folio.presentation") }}" style="color: black;">
                Présentation
            </a>
            <a class="list-group-item d-flex justify-content-center align-items-center @yield("size_com") h-25 title" href="{{ route("folio.competence") }}" style="color: black;">
                Compétences
            </a>
            <a class="list-group-item d-flex justify-content-center align-items-center @yield("size_pro") h-25 title" href="{{ route("project.index") }}" style="color: black;">
                Projets
            </a>
            <a class="list-group-item d-flex justify-content-center align-items-center @yield("size_con") h-25 title" href="{{ route("folio.contact") }}" style="color: black;">
                Contact
            </a>
        </div>
    </div>
</nav>
{{-- <script src="https://cdn.jsdelivr.net/npm/docsearch.js@2/dist/cdn/docsearch.min.js"></script> --}}
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

