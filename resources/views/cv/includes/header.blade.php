<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ route('folio.index') }}">CV-LJA</a>
    @if(Session::has('user'))
    	<a class="navbar-brand col mr-0" href="{{ route('user.show') }}">
    		{{ Session::get('user')->name }} {{ Session::get('user')->surname }}
    	</a>
    	<a class="navbar-brand col mr-0" href="{{ route('log.logout') }}">
    		Déconnexion 
    	</a>
    @else
    	<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ route('log.index') }}">Connexion</a>
    @endif

</nav>
