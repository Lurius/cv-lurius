@extends("cv.includes.template")

@section("content")
    <div class="justify-content-center" align="center">
        <h1 class="title">Langage de programmation :</h1>
    </div>
    @foreach($languages as $language)
        <button class="btn btn-outline-dark">
            @if($language->logo)
                {!! $language->logo !!}
            @else
                <i class="fas fa-tags"></i>
            @endif {{ $language->libelle }}
        </button>
    @endforeach
    <div class="justify-content-center" align="center">
        <h1 class="title">Autres compétences :</h1>
    </div>
    <ul>
        @foreach($competences as $competence)
            <li>{{ $competence->libelle }}</li>
        @endforeach
    </ul>
@endsection

@section("size_com")
h-75
@endsection
